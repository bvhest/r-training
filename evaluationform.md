# Training Evaluation Form


12. What did you like most about this training?
  
    
  
  
13. What aspects of the training could be improved? 
    
  
  

14. How do you hope to change your practice as a result of this training? 
    
  
  

15. What additional adult ESL trainings would you like to have in the future? 
    
  
  

16. Please share other comments or expand on previous responses here:
    
  
  


Thank you for your feedback!
