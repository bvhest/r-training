# R-training

## Intro

This repo contains the directory structure and data that will be used in the R-training.

## Installation

Download the repo as zip-file or use 'git' to clone the repo.
When the zip-file is used, unzip the contents in a directory.

![gitlab](./images/gitlab.png)

Create an R-project that is based on the directory that contains the subdirectories and data.

Steps:

1. open RStudio,
2. from the File-menu, choose "New project",
3. choose "Existing directory",
4. select the directory with the training content,
5. press the button "Create project".
6. done.

When the project-file is opened with RStudio, the base-directory for all operations is the directory with the training content.

Subdirectories or data-files can be accessed with the absolute path ('c:\projects\rtraining\import\iris.csv') or a relative path ('./import/iris.csv').

